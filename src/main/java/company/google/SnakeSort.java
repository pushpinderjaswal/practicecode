package company.google;

public class SnakeSort {

    public int[][] sort(int[][] array){

        for (int row = 0 ; row < array.length ; row++){
            for (int col = 0 ; col < array[0].length ; col++){
                if (row % 2 == 1){
                    sortRighToLeft(array, row);
                    break;
                } else {
                    sortLeftToRight(array, row);
                    break;
                }
            }
        }

        for (int col = 0 ; col < array[0].length ; col++){
            for (int row = 0 ; row < array.length ; row++){
              sortTopToBottom(array, col);
            }
        }

        return array;
    }

    private void sortLeftToRight(int[][] array, int row) {

        for (int col = 0 ; col < array[0].length ; col++) {
            for (int j = col+1 ; j< array[0].length ; j ++) {
                if (array[row][col] > array[row][j]) {
                    int tmp = array[row][col];
                    array[row][col] = array[row][j];
                    array[row][j] = tmp;
                }
            }
        }

    }

    private void sortRighToLeft(int[][] array, int row) {

        for (int col = 0 ; col < array[0].length ; col++) {
            for (int j = col+1 ; j< array[0].length ; j ++) {
                if (array[row][col] < array[row][j]) {
                    int tmp = array[row][col];
                    array[row][col] = array[row][j];
                    array[row][j] = tmp;
                }
            }
        }

    }

    private void sortTopToBottom(int[][] array, int col) {

        for (int row = 0 ; row < array.length ; row++) {
            for (int j = row+1 ; j< array.length ; j ++) {
                if (array[row][col] > array[row][j]) {
                    int tmp = array[row][col];
                    array[row][col] = array[row][j];
                    array[row][j] = tmp;
                }
            }
        }

    }

    public static void main(String[] args) {

        int[][] array = new int[3][3];

        array[0][0] = 1; array[0][1] = 5; array[0][2] = 3;
        array[1][0] = 8; array[1][1] = 7; array[1][2] = 2;
        array[2][0] = 4; array[2][1] = 6; array[2][2] = 9;

        System.out.println(1 % 2);

        SnakeSort snakeSort = new SnakeSort();

        int[][] result = snakeSort.sort(array);
        System.out.println(result.toString());

       for (int row = 0 ; row < result.length ; row++) {
           for (int col = 0 ; col < result[0].length ; col++) {
               System.out.print(" " + result[row][col] + " ");
           }
           System.out.println();
       }

    }
}
