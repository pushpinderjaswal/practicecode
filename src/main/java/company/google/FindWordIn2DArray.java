package company.google;

public class FindWordIn2DArray {

    public int findWord(char[][] matrix, String word) {

        int countHor = 0;
        int countVer = 0;

        for (int i = 0; i < matrix.length ; i++) {
            for (int j = 0; j < matrix[0].length ; j++) {
                if (matrix[i][j] == word.charAt(0)) {
                    int rowIndex = j+1;
                    int charIndex = 1;
                    while(rowIndex < word.length() + j) {
                        if (matrix[i][rowIndex] == word.charAt(charIndex)){
                            rowIndex++;
                            charIndex++;
                        } else {
                            break;
                        }
                    }
                    if (charIndex == word.length()){
                        countHor++;
                    }
                }
            }
        }

        for (int col = 0; col < matrix.length ; col++) {
            for (int row = 0; row < matrix[0].length ; row++) {
                if (matrix[row][col] == word.charAt(0)){
                    int rowIndex = row+1;
                    int wordIndex = 1;
                    while (rowIndex < word.length() + row && rowIndex <matrix[0].length) {
                        if (matrix[rowIndex][col] == word.charAt(wordIndex)) {
                            wordIndex++;
                            rowIndex++;
                        } else {
                            break;
                        }
                    }
                    if (wordIndex == word.length()) {
                        countVer++;
                    }
                }
            }
        }

        return countHor + countVer;
    }


    public static void main(String[] args) {
        FindWordIn2DArray findWordIn2DArray = new FindWordIn2DArray();

        char[][] array = new char[3][3];

        array[0][0] = 'C'; array[0][1] = 'A'; array[0][2] = 'Z';
        array[1][0] = 'A'; array[1][1] = 'C'; array[1][2] = 'A';
        array[2][0] = 'J'; array[2][1] = 'A'; array[2][2] = 'L';

        System.out.println(findWordIn2DArray.findWord(array, "CA"));
    }
}
