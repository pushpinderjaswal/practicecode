package company.google;

import dataStructures.graphs.GraphNode;

import java.util.Iterator;

public class CompareTwoTreesDFS {

    public boolean compare(GraphNode root1, GraphNode root2) {

        if (root1 == null && root2 == null){
            return true;
        }
        if (root1.item != root2.item) {
            return false;
        }

        if (root1 != null && root2 != null){
            root1.visited = true;
            root2.visited = true;

            Iterator<GraphNode> iterator1 = root1.children.iterator();
            Iterator<GraphNode> iterator2 = root2.children.iterator();

            while (iterator1.hasNext() && iterator2.hasNext()) {
                GraphNode node1 = iterator1.next();
                GraphNode node2 = iterator2.next();
                if (!node1.visited && !node2.visited) {
                    compare(node1, node2);
                }
            }
        }

        return root1.item == root2.item;
    }

    public static void main(String[] args) {

        GraphNode one = new GraphNode(1);
        GraphNode two = new GraphNode(2);
        GraphNode three = new GraphNode(3);
        GraphNode four = new GraphNode(4);
        GraphNode five = new GraphNode(5);
        GraphNode six = new GraphNode(6);
        GraphNode seven = new GraphNode(7);
        GraphNode eight = new GraphNode(8);

        one.children.add(two);
        one.children.add(four);
        one.children.add(six);

        two.children.add(three);
        four.children.add(five);
        six.children.add(seven);

        three.children.add(five);
        three.children.add(two);

        five.children.add(three);
        five.children.add(four);
        five.children.add(eight);

        seven.children.add(eight);
        seven.children.add(six);

        eight.children.add(five);
        eight.children.add(seven);

        GraphNode one1 = new GraphNode(1);
        GraphNode two1 = new GraphNode(2);
        GraphNode three1 = new GraphNode(3);
        GraphNode four1 = new GraphNode(4);
        GraphNode five1 = new GraphNode(5);
        GraphNode six1 = new GraphNode(6);
        GraphNode seven1 = new GraphNode(7);
        GraphNode eight1 = new GraphNode(8);

        one1.children.add(two1);
        one1.children.add(four1);
        one1.children.add(six1);

        two1.children.add(three1);
        four1.children.add(five1);
        six1.children.add(seven1);

        three1.children.add(five1);
        three1.children.add(two1);

        five1.children.add(three1);
        five1.children.add(four1);
        five1.children.add(eight1);

        seven1.children.add(eight1);
        seven1.children.add(six1);

        eight1.children.add(five1);
        eight1.children.add(seven1);

        CompareTwoTreesDFS compareTwoTreesDFS = new CompareTwoTreesDFS();

        System.out.println(compareTwoTreesDFS.compare(one, one1));
    }

}
