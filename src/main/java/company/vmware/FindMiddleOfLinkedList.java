package company.vmware;

public class FindMiddleOfLinkedList {
     static class LinkedList {
         Node head;

         class Node {
             int data;
             Node next;

             Node(int d) {
                 this.data = d;
             }
         }

         public void push(int newData) {
             Node node = new Node(newData);
             node.next = head;
             head = node;
         }

         public void printList() {
             Node node = head;
             while (node != null) {
                 System.out.print(node.data);
                 node = node.next;
             }
         }
     }

     public void getMiddleOfLinkedList (LinkedList.Node head) {

         LinkedList.Node slow_ptr = head;
         LinkedList.Node fast_ptr = head;

         if (head != null) {
             while (fast_ptr != null && fast_ptr.next != null) {
                 fast_ptr = fast_ptr.next.next;
                 slow_ptr = slow_ptr.next;
             }
             System.out.print(slow_ptr.data);
         }

     }

    public static void main(String[] args) {

        FindMiddleOfLinkedList findMiddleOfLinkedList = new FindMiddleOfLinkedList();
        LinkedList linkedList = new LinkedList();

        for (int i = 5 ; i > 0 ; i--) {
            linkedList.push(i);
            linkedList.printList();
            System.out.println();
            findMiddleOfLinkedList.getMiddleOfLinkedList(linkedList.head);
            System.out.println();
        }

    }
}
