package company.vmware.interviewQuestion;

public class IsPower {

    public boolean isPowerOfThree(int x, int num) {

        int pow = 1;
        while (pow < num) {
            pow = pow * x;
        }
        if(pow == num) {
            return true;
        }

        return false;
    }

    public static void main(String[] args) {

        IsPower isPower = new IsPower();

        System.out.println(isPower.isPowerOfThree(3, 27));
        System.out.println(isPower.isPowerOfThree(3, 9));

    }
}
