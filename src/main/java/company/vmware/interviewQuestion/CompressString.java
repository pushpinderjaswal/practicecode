package company.vmware.interviewQuestion;

/**
 Given a string, write a function to compress it by shortening every sequence of the same character
 to that character followed by the number of repetitions. If the compressed string is longer
 than the original, you should return the original string.

 Compress(“a”) = a
 Compress(“aaa”) = a3
 Compress(“aaabbb”) = a3b3
 Compress(“aaabccc”) = a3b1c3
 Compress(“aaabcccbbbb”) = a3b1c3b4
 */

public class CompressString {

    public String returnCompressedString (String input) {

        StringBuilder sb = new StringBuilder();

        if(input == null || input == "") {
            return null;
        }

        if (input.length() == 1) {
            return input;
        }

        int count = 1;
        int index = 0;

        for (int i = 1; i < input.length() ; i++) {
            if (input.charAt(i) == input.charAt(i - 1)) {
                count = count + 1;
            } else {
                sb.append(input.charAt(i-1));
                sb.append(count);
                count = 1;
            }
            index++;
        }

        if (index == input.length() - 1) {
            sb.append(input.charAt(index));
            sb.append(count);
        }

        if (sb.length() > input.length()) {
            return input;
        }

        return sb.toString();
    }
}
