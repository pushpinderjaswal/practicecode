package company.vmware.interviewQuestion;

public class PrimeNumber {

    public void printPrimeNumbers(int limit) {

        for(int i = 1; i <= limit ; i++) {
            for (int j = 1 ; j <= i ; j++) {
                if(i%j == 0) {
                    System.out.println(i);
                    break;
                }
            }
        }
    }
}
