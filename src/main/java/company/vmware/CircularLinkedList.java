package company.vmware;

import dataStructures.linkedList.LinkedListNode;

public class CircularLinkedList {

    public boolean isCircular(LinkedListNode head) {
        LinkedListNode slow = head;
        LinkedListNode fast = head.nextNode;

        while (fast.nextNode != null && fast.nextNode.nextNode != null) {
            slow = slow.nextNode;
            fast = fast.nextNode.nextNode;
            if (slow.item == fast.item) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        CircularLinkedList circularLinkedList = new CircularLinkedList();
        LinkedListNode head = new LinkedListNode(0);
        LinkedListNode one = new LinkedListNode(1);
        LinkedListNode two = new LinkedListNode(2);
        LinkedListNode three = new LinkedListNode(3);
        LinkedListNode four = new LinkedListNode(4);

        head.nextNode = one;
        one.nextNode = two;
        two.nextNode = three;
        three.nextNode = four;

        four.nextNode = null;

        System.out.println(circularLinkedList.isCircular(head));
    }
}
