package company.vmware;



import dataStructures.tree.BinaryTreeNode;

public class FindDepthOfTree {

    public int findDepth(BinaryTreeNode root) {
        if(root==null)
            return 0;

        int leftDepth = findDepth(root.left);
        int rightDepth = findDepth(root.right);

        int bigger = Math.max(leftDepth, rightDepth);

        return bigger+1;
    }

    public int findDepth2(BinaryTreeNode root) {

        if (root == null) {
            return 0;
        }

        return Math.max(findDepth2(root.left), findDepth2(root.right)) + 1;

    }

    public static void main(String[] args) {
        FindDepthOfTree findDepthOfTree = new FindDepthOfTree();

        BinaryTreeNode root = new BinaryTreeNode(4);

        BinaryTreeNode rightRightRight = new BinaryTreeNode(8, null, null, null);
        BinaryTreeNode rightRight = new BinaryTreeNode(7, null, rightRightRight, null);
        BinaryTreeNode rightLeft = new BinaryTreeNode(5, null, null, null);
        BinaryTreeNode leftRight = new BinaryTreeNode(3, null, null, null);
        BinaryTreeNode leftLeft = new BinaryTreeNode(1, null, null, null);
        BinaryTreeNode right = new BinaryTreeNode(6, rightLeft, rightRight, null);
        BinaryTreeNode left = new BinaryTreeNode(2, leftLeft, leftRight, root);

        root.parent = null;
        root.left = left;
        root.right = right;

        leftLeft.parent = left;
        leftRight.parent = left;

        rightLeft.parent = right;
        rightRight.parent = right;

        System.out.println(findDepthOfTree.findDepth(root));

        System.out.println(findDepthOfTree.findDepth2(root));
    }
}
