package company.vmware;

import java.util.Arrays;

public class IsUnique {

    private boolean isUnique(String input) {

        int MAX_CHARS = 265;

        if (input.length() > MAX_CHARS) {
            return false;
        }

        boolean[]  chars = new boolean[MAX_CHARS];
        Arrays.fill(chars, false);

        for (int i = 0; i < input.length() ; i++) {

            int index = input.charAt(i);
            System.out.println(index);

            if (chars[index])
                return false;

            chars[index] = true;

        }
        return true;
    }

    public static void main(String[] args) {
        IsUnique isUnique = new IsUnique();
        System.out.println(isUnique.isUnique("pushp"));
    }

}
