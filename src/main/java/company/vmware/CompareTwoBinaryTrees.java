package company.vmware;

import dataStructures.tree.BinaryTreeNode;

public class CompareTwoBinaryTrees {

    public boolean compare(BinaryTreeNode root1, BinaryTreeNode root2) {

        if (root1 == null && root2 == null) {
            return true;
        }

        return root1.item == root2.item && compare(root1.left, root2.left) && compare(root1.right, root2.right);
    }


    public static void main(String[] args) {

        BinaryTreeNode root = new BinaryTreeNode(4);

        BinaryTreeNode rightRightRight = new BinaryTreeNode(8, null, null, null);
        BinaryTreeNode rightRight = new BinaryTreeNode(7, null, rightRightRight, null);
        BinaryTreeNode rightLeft = new BinaryTreeNode(5, null, null, null);
        BinaryTreeNode leftRight = new BinaryTreeNode(3, null, null, null);
        BinaryTreeNode leftLeft = new BinaryTreeNode(1, null, null, null);
        BinaryTreeNode right = new BinaryTreeNode(6, rightLeft, rightRight, null);
        BinaryTreeNode left = new BinaryTreeNode(2, leftLeft, leftRight, root);

        root.parent = null;
        root.left = left;
        root.right = right;

        leftLeft.parent = left;
        leftRight.parent = left;

        rightLeft.parent = right;
        rightRight.parent = right;

        BinaryTreeNode root1 = new BinaryTreeNode(4);

        BinaryTreeNode rightRightRight1 = new BinaryTreeNode(8, null, null, null);
        BinaryTreeNode rightRight1 = new BinaryTreeNode(7, null, rightRightRight1, null);
        BinaryTreeNode rightLeft1 = new BinaryTreeNode(5, null, null, null);
        BinaryTreeNode leftRight1 = new BinaryTreeNode(3, null, null, null);
        BinaryTreeNode leftLeft1 = new BinaryTreeNode(1, null, null, null);
        BinaryTreeNode right1 = new BinaryTreeNode(6, rightLeft1, rightRight1, null);
        BinaryTreeNode left1 = new BinaryTreeNode(2, leftLeft1, leftRight1, root1);

        root1.parent = null;
        root1.left = left1;
        root1.right = right1;

        leftLeft1.parent = left1;
        leftRight1.parent = left1;

        rightLeft1.parent = right1;
        rightRight1.parent = right1;

        CompareTwoBinaryTrees compareTwoBinaryTrees = new CompareTwoBinaryTrees();

        System.out.println(compareTwoBinaryTrees.compare(root, root1));

    }
}
