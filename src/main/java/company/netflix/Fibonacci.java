package company.netflix;

import dataStructures.arraysAndStrings.FindDups;

/**
 * 0 1 1 2 3 5 8 ...
 */
public class Fibonacci {

    public int getFibonacci(int limit) {

        if (limit == 0) {
            return 0;
        }
        if (limit == 1) {
            return 1;
        }

        return getFibonacci(limit - 1 ) + getFibonacci(limit - 2);
    }

    public static void main(String[] args) {
        Fibonacci fibonacci = new Fibonacci();

        System.out.println(fibonacci.getFibonacci(4));
    }
}
