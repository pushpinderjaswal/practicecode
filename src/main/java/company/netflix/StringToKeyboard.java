package company.netflix;

import java.util.HashMap;
import java.util.Map;

public class StringToKeyboard {

    private char[][] keyboard = new char[6][5];
    private int counter = 48;
    private String row = "row";
    private String col = "col";

    private char[][] createKeyboard(){

        for (int i = 0; i<keyboard.length ; i++){
            for (int j = 0; j< 5; j++){
                keyboard[i][j] = (char)counter;
                counter++;
            }
        }
        return keyboard;
    }

    private void up(Map<String, Integer> map){
        int temp = map.get(row);
        map.put(row, temp-1);
    }

    private void down(Map<String, Integer> map){
        int temp = map.get(row);
        map.put(row, temp+1);
    }

    private void left(Map<String, Integer> map){
        int temp = map.get(col);
        map.put(col, temp-1);
    }

    private void right(Map<String, Integer> map){
        int temp = map.get(col);
        map.put(col, temp+1);
    }

    private void findPath(String input){
        char[] charArray = input.toCharArray();
        Map<String, Integer> source = findPosition(charArray[0]);

        for(int i = 1; i< charArray.length ; i++){
            Map<String, Integer> dest = findPosition(charArray[i]);
            System.out.println("Directions from: " + charArray[i-1] + " -> " + charArray[i]);
            while (source.get(row) != dest.get(row) || source.get(col) != dest.get(col)){
                if(source.get(col) < dest.get(col)){
                    System.out.println("right");
                    right(source);
                } else if (source.get(col) > dest.get(col)){
                    System.out.println("left");
                    left(source);
                } else if (source.get(row) > dest.get(row)){
                    System.out.println("up");
                    up(source);
                } else if (source.get(row) < dest.get(row)){
                    System.out.println("down");
                    down(source);
                }
            }
            System.out.println();
            source = dest;
        }
    }

    private Map<String, Integer> findPosition(char c){

        Map<String, Integer> map = new HashMap<String, Integer>();
        for (int i = 0; i<keyboard.length ; i++){
            for (int j = 0; j< 5; j++){
                if (keyboard[i][j] == c){
                    map.put(row,i);
                    map.put(col,j);
                    return map;
                }
            }
        }
        return map;
    }

    private static void print(char[][] keyboard) {

        for (int i = 0; i<keyboard.length ; i++){
            for (int j = 0; j<5 ; j++){
                System.out.print(keyboard[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {

        StringToKeyboard obj = new StringToKeyboard();
        print(obj.createKeyboard());
        System.out.println();
        obj.findPath("108");
    }
}
