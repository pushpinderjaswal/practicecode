package company.amazon;

import dataStructures.tree.BinaryTree;
import dataStructures.tree.BinaryTreeNode;

public class DeleteLeafNode {

    public BinaryTreeNode deleteLeafNodes(BinaryTreeNode root) {

        BinaryTreeNode result = root;

        if (root == null) {
            return result;
        } else {
            if (root.left == null && root.right == null) {
                root.item = 0;
                return result;
            }

            if (root.left != null) {
                deleteLeafNodes(root.left);
            }
            if (root.right != null) {
                deleteLeafNodes(root.right);
            }
        }



        return result;
    }

    public static void main(String[] args) {
        DeleteLeafNode deleteLeafNode = new DeleteLeafNode();
        BinaryTree binaryTree = new BinaryTree();
        BinaryTreeNode root = new BinaryTreeNode(4);

        BinaryTreeNode rightRight = new BinaryTreeNode(7, null, null, null);
        BinaryTreeNode rightLeft = new BinaryTreeNode(5, null, null, null);
        BinaryTreeNode leftRight = new BinaryTreeNode(3, null, null, null);
        BinaryTreeNode leftLeft = new BinaryTreeNode(1, null, null, null);
        BinaryTreeNode right = new BinaryTreeNode(6, rightLeft, rightRight, null);
        BinaryTreeNode left = new BinaryTreeNode(2, leftLeft, leftRight, root);

        root.parent = null;
        root.left = left;
        root.right = right;

        leftLeft.parent = left;
        leftRight.parent = left;

        rightLeft.parent = right;
        rightRight.parent = right;
//
//        leftLeft.left = new BinaryTreeNode(8);
//        leftLeft.right = new BinaryTreeNode(9);
//        leftRight.left = new BinaryTreeNode(10);
//        leftRight.right = new BinaryTreeNode(11);
//
//        rightLeft.left = new BinaryTreeNode(12);
//        rightLeft.right = new BinaryTreeNode(13);
//        rightRight.left = new BinaryTreeNode(14);
//        rightRight.right = new BinaryTreeNode(15);

        System.out.println("Inorder traversal:");
        binaryTree.inOrderTraversal(root);

        BinaryTreeNode result = deleteLeafNode.deleteLeafNodes(root);

        System.out.println("Inorder traversal:");
        binaryTree.inOrderTraversal(result);
    }
}
