package company.amazon;

public class FindSubstring {

    public boolean find(String input, String subString){

        for (int i=0 ; i<input.length() ; i++){
            if (input.charAt(i) == subString.charAt(0)) {
                int index = 1;
                for (int j = i+1; j <i + subString.length() ; j++){
                    if (input.charAt(j) != subString.charAt(index)){
                        break;
                    }
                    index++;
                }
                if (index == subString.length()){
                    return true;
                }
            }
        }
        return false;
    }

    public static void main(String[] args) {
        FindSubstring findSubstring = new FindSubstring();
        System.out.println(findSubstring.find("icdjkcdojessiaiowcd", "jessica"));
    }
}
