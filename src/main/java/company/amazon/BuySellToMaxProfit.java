package company.amazon;

import java.util.ArrayList;
import java.util.List;

public class BuySellToMaxProfit {

    class Interval {
        int buy;
        int sell;

        @Override
        public String toString() {
            return "Interval{" +
                    "buy=" + buy +
                    ", sell=" + sell +
                    '}';
        }
    }

    public List<Interval> stockBuySell(int[] input) {

        int length = input.length - 1;

        if (input.length < 1){
            return null;
        }

        List<Interval> intervals = new ArrayList<>();
        int index = 0;

        while (index < length){

            while (index < length && input[index + 1] <= input[index]) {
                index++;
            }

            if (index == length){
                break;
            }

            Interval interval = new Interval();
            interval.buy = index++;

            while (index < length && input[index + 1] >= input[index]){
                index++;
            }

            interval.sell = index++;

            intervals.add(interval);
        }

        return intervals;
    }

    public static void main(String[] args) {

        BuySellToMaxProfit buySellToMaxProfit = new BuySellToMaxProfit();

        int[] input = {100,180,260,310,40,535,695};

        List<Interval> intervals = buySellToMaxProfit.stockBuySell(input);
        System.out.println(intervals);
    }

}
