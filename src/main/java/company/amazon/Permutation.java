package company.amazon;

public class Permutation {

    public void permute(String input, int l, int r) {
        if (l == r){
            System.out.println(input);
        } else {
            for (int i = l; i <= r ; i++){
                input = swap(input, l,i);
                permute(input, l + 1, r);
//                input = swap(input, l, i);
            }
        }
    }

    private String swap(String input, int l, int i) {
        char[] inputArray = input.toCharArray();
        char temp  = inputArray[l];
        inputArray[l] = inputArray[i];
        inputArray[i] = temp;

        return String.valueOf(inputArray);
    }

    public static void main(String[] args) {
        Permutation permutation = new Permutation();

        permutation.permute("1234", 0, 3  );
    }

}
