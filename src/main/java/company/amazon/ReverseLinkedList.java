package company.amazon;

import dataStructures.linkedList.LinkedListNode;

public class ReverseLinkedList {

    LinkedListNode reverse(LinkedListNode node) {

        LinkedListNode prev = null;
        LinkedListNode current = node;
        LinkedListNode next = null;
        while (current != null) {
            next = current.nextNode;
            current.nextNode = prev;
            prev = current;
            current = next;
        }
        node = prev;
        return node;
    }


    LinkedListNode reverse(LinkedListNode head, int k)
    {
        LinkedListNode current = head;
        LinkedListNode next = null;
        LinkedListNode prev = null;

        int count = 0;

       /* Reverse first k nodes of linked list */
        while (count < k || current != null)
        {
            next = current.nextNode;
            current.nextNode = prev;
            prev = current;
            current = next;
            count++;
        }

       /* next is now a pointer to (k+1)th node
          Recursively call for the list starting from current.
          And make rest of the list as next of first node */
        if (next != null)
            prev.nextNode = reverse(next, k);

        // prev is now head of input list
        return prev;
    }

    void printList(LinkedListNode node) {
        while (node != null) {
            System.out.print(node.item + " ");
            node = node.nextNode;
        }
    }

    public static void main(String[] args) {

        LinkedListNode list = new LinkedListNode();
        list.item = 1;
        list.nextNode = new LinkedListNode(2);
        list.nextNode.nextNode = new LinkedListNode(3);
        list.nextNode.nextNode.nextNode = new LinkedListNode(4);
        list.nextNode.nextNode.nextNode.nextNode = new LinkedListNode(5);
        list.nextNode.nextNode.nextNode.nextNode.nextNode = null;



        ReverseLinkedList reverseLinkedList = new ReverseLinkedList();

        reverseLinkedList.printList(list);
        System.out.println();

        LinkedListNode result = reverseLinkedList.reverse(list);

        reverseLinkedList.printList(result);


        reverseLinkedList.reverse(list, 3);

        LinkedListNode result2 = reverseLinkedList.reverse(list);

        System.out.println();
        reverseLinkedList.printList(result2);

    }

}
