package company.amazon;

import dataStructures.linkedList.DoublyLinkedListNode;

import java.util.HashMap;
import java.util.Map;

public class LRUCache {

    DoublyLinkedListNode listHead = null;
    DoublyLinkedListNode listTail = null;

    Map<Integer,DoublyLinkedListNode> map = new HashMap<>();
    private int maxCacheSize;

    public LRUCache(int maxCacheSize){
        this.maxCacheSize = maxCacheSize;
    }

    public String getKey(int key) {
        DoublyLinkedListNode node = map.get(key);

        if (node == null) {
            return null;
        }

        if (node != listHead){
            removeFromList(node);
            insertInFront(node);
        }
        return node.value;
    }

    private void insertInFront(DoublyLinkedListNode node) {
        if (node == null) {
            return;
        }
        if (listHead == null) {
            listHead = node;
            listTail = node;
        } else {
            node.next = listHead;
            listHead.previous = node;
            listHead = node;
        }
    }

    private void removeFromList(DoublyLinkedListNode node) {
        if (node == null) {
            return;
        }
        if (node.next != null){
            node.next.previous = node.previous;
        }
        if (node.previous != null) {
            node.previous.next = node.next.next;
        }
        if (node == listHead) {
            listHead = node.next;
        }
        if (node == listTail) {
            listTail = node.previous;
        }
    }

    private boolean removeKey(int key) {
        DoublyLinkedListNode node = map.get(key);
        removeFromList(node);
        map.remove(key);
        return true;
    }

    private void setKeyValue(int key, String value){
        removeKey(key);

        if (map.size() >= maxCacheSize && listTail !=null){
            removeKey(listTail.key);
        }

        DoublyLinkedListNode node = new DoublyLinkedListNode(key, value);
        insertInFront(node);
        map.put(key, node);
    }

}
