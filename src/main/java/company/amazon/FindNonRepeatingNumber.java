package company.amazon;

import java.util.ArrayList;
import java.util.List;

public class FindNonRepeatingNumber {

    public List<Integer> find(int[] input) {
        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < input.length ; i++) {
            if (list.contains(input[i])){
                Integer temp = input[i];
                list.remove(temp);
            } else {
                list.add(input[i]);
            }
        }
        return list;
    }

    public static void main(String[] args) {
        FindNonRepeatingNumber findNonRepeatingNumber = new FindNonRepeatingNumber();
        int[] input = {1,3,2,5,1,6,3,2,6};

        System.out.println(findNonRepeatingNumber.find(input));
    }

}
