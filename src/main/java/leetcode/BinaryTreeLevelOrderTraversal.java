package leetcode;

import dataStructures.tree.BinaryTreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BinaryTreeLevelOrderTraversal {

    List<List<Integer>> resultList = new ArrayList<>();

    public List<List<Integer>> levelOrder(TreeNode root) {

        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);

        int counter = 0;
        int power = 2;
        List<Integer> list = new LinkedList<>();
        list.add(root.val);
        resultList.add(list);
        list.clear();

        while (root != null) {
//            counter = counter + 1;
            if (counter == power) {
                resultList.add(list);
                list.clear();
                power = power * 2;
            }
            counter = counter + 1;
            TreeNode node = queue.remove();
            list.add(node.val);
            queue.add(node.left);
            queue.add(node.right);
        }
        return resultList;
    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    public static void main(String[] args) {
        BinaryTreeLevelOrderTraversal binaryTreeLevelOrderTraversal = new BinaryTreeLevelOrderTraversal();


        TreeNode root = new TreeNode(4);

        TreeNode rightRight = new TreeNode(7);
        TreeNode rightLeft = new TreeNode(5);
        TreeNode leftRight = new TreeNode(3);
        TreeNode leftLeft = new TreeNode(1);
        TreeNode right = new TreeNode(6);
        TreeNode left = new TreeNode(2);

        root.left = left;
        root.right = right;

        left.left = leftLeft;
        left.right = leftRight;
        right.left = rightLeft;
        right.right = rightRight;

        binaryTreeLevelOrderTraversal.levelOrder(root);
    }
}
