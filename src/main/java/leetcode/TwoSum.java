package leetcode;

/**
 * Created by pjaswa1 on 7/10/17.
 */
public class TwoSum {


        public static int[] twoSum(int[] nums, int target) {

            int[] result = new int[2];

            for(int i=0; i < nums.length; i++){

                for(int j=i+1; j<nums.length; j++){
                    if(target - nums[j] == nums[i] ){
                        result[0] = i;
                        result[1] = j;
                        return result;
                    }
                }
            }
            return result;
        }

    public static void main(String[] args) {

        int[] test = {3,2,4};

        System.out.println(twoSum(test, 6));



    }

}
