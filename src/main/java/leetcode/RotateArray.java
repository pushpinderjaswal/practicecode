package leetcode;

import java.util.Arrays;

public class RotateArray {

    public int[] rotate(int[] input, int k){

        if (input == null | input.length == 0 || k < 0 ){
            throw new IllegalArgumentException("Illegal argument");
        }

        int lengthOfFirstHalf = input.length - k;

        reverse(input, 0, lengthOfFirstHalf - 1);
        reverse(input, lengthOfFirstHalf, input.length - 1);
        reverse(input, 0, input.length - 1);

        return input;
    }

    public void reverse(int[] array, int left, int right) {

        if (array == null || array.length == 1) {
            return;
        }

        while (left < right) {
            int temp = array[left];
            array[left] = array[right];
            array[right] = temp;
            left++;
            right--;
        }

    }

    public static void main(String[] args) {
        RotateArray rotateArray = new RotateArray();

        int[] array = {1,2,3,4,5,6,7};

        int[] result = rotateArray.rotate(array, 2);


        System.out.println(Arrays.toString(result));
//        for (int i = 0; i<result.length ; i++) {
//            System.out.println();
//        }
    }
}
