package leetcode;

public class MaxSumSubarray {

    public int maxSubArray(int[] nums) {

        int windowSize = 1;
        int maxSum = 0;
        boolean firstTime = true;

        if (nums.length < 2){
            return nums[0];
        }

        while (windowSize <= nums.length){
            for (int i = 0; i<nums.length ; i++){
                if (i + windowSize > nums.length){
                    break;
                }
                int localSum = 0;
                for (int j = i ; j < i + windowSize ; j ++){
                    localSum = localSum + nums[j];
                }

                if (firstTime) {
                    maxSum = localSum;
                    firstTime = false;
                }
                if (localSum > maxSum){
                    maxSum = localSum;
                }
            }
            windowSize++;
        }
        return maxSum;
    }

    public static void main(String[] args) {

        MaxSumSubarray maxSumSubarray = new MaxSumSubarray();

        int[] inputArray = {1,2};

        System.out.println(maxSumSubarray.maxSubArray(inputArray));

    }
}
