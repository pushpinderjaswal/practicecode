package leetcode;


/**
 * Created by pjaswa1 on 7/10/17.
 */
public class AddTwoNumbers {



    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {

        if(l1.next == null || l2.next == null){
            int sum = l1.val + l2.val;
            l1.val = sum;
            if (sum >= 10 && l1.next != null){
                l1.next.val++;
                l1.val = sum - 10;

            } else if (sum >= 10 && l1.next == null){
                l1.next = new ListNode(1);
                l1.val = sum - 10;
            }
            return l1;
        }

        int sum = l1.val + l2.val;
        l1.val = sum;
        if (sum >= 10 && l1.next != null){
            l1.next.val++;
            l1.val = sum - 10;

        }
        addTwoNumbers(l1.next, l2.next);

        return l1;
    }

    public static void main(String[] args) {

        ListNode l1 = new ListNode(2);
        l1.next = new ListNode(4);
        l1.next.next = new ListNode(3);
        l1.next.next.next = null;

        ListNode l2 = new ListNode(5);
        l2.next = new ListNode(6);
        l2.next.next = new ListNode(4);
        l2.next.next.next = null;

        AddTwoNumbers addTwoNumbers = new AddTwoNumbers();

        ListNode result = addTwoNumbers.addTwoNumbers(l1,l2);

        System.out.println(result.val);
        System.out.println(result.next.val);
        System.out.println(result.next.next.val);

    }
}
