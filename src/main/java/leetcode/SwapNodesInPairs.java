package leetcode;

public class SwapNodesInPairs {


    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }

    static class Solution {
        public ListNode swapPairs(ListNode head) {
            ListNode resultHead = new ListNode(0);
            resultHead.next = head;
            int counter = 0;
            while (head != null && head.next != null) {
                    ListNode second = head.next;
                    head.next = head.next.next;
                    second.next = head;
                    counter++;
                    if (counter == 1) {
                        resultHead = second;
                    }
                    head = head.next.next;
                }
            return resultHead;
        }
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        ListNode one = new ListNode(1);
        ListNode two = new ListNode(2);
        ListNode three = new ListNode(3);
        ListNode four = new ListNode(4);
        ListNode five = new ListNode(5);
        ListNode six = new ListNode(6);

        one.next = two;
        two.next = three;
        three.next = four;
        four.next = five;
        five.next = six;
        six.next = null;
        ListNode result = solution.swapPairs(one);

        while (result != null) {
            System.out.println(result.val);
            result = result.next;
        }

    }
}
