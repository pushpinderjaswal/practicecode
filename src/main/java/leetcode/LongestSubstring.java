package leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by pjaswa1 on 7/11/17.
 */
public class LongestSubstring {

    public int lengthOfLongestSubstring(String s) {
        char[] charArray = s.toCharArray();
        Map<Character, Integer> map = new HashMap<Character, Integer>();

        for (int i=0 ; i<charArray.length ; i++){
            if(map.containsKey(charArray[i])){
                map.clear();
            }
            map.put(charArray[i], 1);
        }

        return map.size() - 1;
    }

    public int lengthOfLongestSubstring1(String s) {

        int max = 0;
        char[] charArray = s.toCharArray();
        List<Character> list = new ArrayList<Character>();

        for (int i = 0 ; i < charArray.length; i++){
            if(list.contains(charArray[i])){
                if (max < list.size()){
                    max = list.size();
                }
                char temp = list.get(list.size() - 1);
                list.clear();
                list.add(temp);
            } else {
                list.add(charArray[i]);
                if (i == charArray.length-1){
                    if (max < list.size()){
                        max = list.size();
                    }
                }
            }
        }
        return max;
    }

    public static void main(String[] args) {

        LongestSubstring longestSubstring = new LongestSubstring();

        System.out.println(longestSubstring.lengthOfLongestSubstring1("aab"));

    }
}
