package leetcode;

public class ReverseInteger {

    public int reverse(int input)
    {
        long reversedNum = 0;
        long input_long = input;

        while (input_long != 0)
        {
            reversedNum = reversedNum * 10 + input_long % 10;
            input_long = input_long / 10;
        }

        if (reversedNum > Integer.MAX_VALUE || reversedNum < Integer.MIN_VALUE)
        {
            return 0;
        }
        return (int)reversedNum;
    }

    static int minCoins(int coins[], int m, int V)
    {
        // base case
        if (V == 0) return 0;

        // Initialize result
        int res = Integer.MAX_VALUE;

        // Try every coin that has smaller value than V
        for (int i=0; i<m; i++)
        {
            if (coins[i] <= V)
            {
                int sub_res = minCoins(coins, m, V-coins[i]);

                // Check for INT_MAX to avoid overflow and see if
                // result can minimized
                if (sub_res != Integer.MAX_VALUE && sub_res + 1 < res)
                    res = sub_res + 1;
            }
        }

        if (res == 0 ){
            return -1;
        }

        return res;
    }

    public boolean isPalindrome(int x) {

        return reverse(x) == x;
    }

    public static void main(String[] args) {

        ReverseInteger reverseInteger = new ReverseInteger();

        System.out.println(reverseInteger.reverse(123));

        System.out.println(reverseInteger.isPalindrome(-2147447412));

    }
}
