package leetcode;

/**
 * Created by pjaswa1 on 7/10/17.
 */
public class ListNode {

    int val;
    ListNode next;

    ListNode(int x) { val = x; }
}
