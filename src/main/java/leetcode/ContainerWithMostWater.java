package leetcode;

public class ContainerWithMostWater {

    public int maxArea(int[] height) {

        int maxArea = 0;

        for (int i=0 ; i<height.length ; i++){
            for (int j=i+1 ; j<height.length ; j++) {
                int area = height[i] * height[j];
                if (area > maxArea) {
                    maxArea = area;
                }
            }

        }

        return maxArea;
    }

    public static void main(String[] args) {
        ContainerWithMostWater containerWithMostWater = new ContainerWithMostWater();

        int[] input = {1,2};
        System.out.println(containerWithMostWater.maxArea(input));
    }
}
