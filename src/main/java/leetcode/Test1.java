package leetcode;

import java.util.*;

public class Test1 {

    static int lonelyInteger(int[] arr) {

        Map<Integer, Boolean> map = new HashMap<>();
        int result = 0;
        int len = arr.length;

        for (int i = 0 ; i< arr.length ; i++){
            if(map.containsKey(arr[i])){
                map.put(arr[i], false);
            } else {
                map.put(arr[i], true);
            }
        }

        for (Map.Entry<Integer, Boolean> entry: map.entrySet()){
            if(entry.getValue() == true){
                result =  entry.getKey();
            }
        }
        return result;
    }

//        static int lonelyInteger(int[] arr) {
//
//
//
//        }

    public static void main(String[] args) {

        int[] test = {1,1,2};
        System.out.println(lonelyInteger(test));
    }




}
