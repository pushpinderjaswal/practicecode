package leetcode;

public class LongestPalindromicSubstring {

    public String longestPalindrome(String s) {

        StringBuffer longestPalindrome = null;

        if (s.length() <=1){
            return s;
        }

        char[] inputArray = s.toCharArray();
        int length = inputArray.length;

        for(int i = 0; i < length ; i++){
            for(int j = length - 1; j > i ; j--){
                if(inputArray[i]  == inputArray[j]) {
                    if(checkIfPalindrome(inputArray, i, j)){
                        StringBuffer tempBf = new StringBuffer();
                        for (int m = i; m <= j ; m++){
                            tempBf.append(inputArray[m]);
                        }
                        if(longestPalindrome == null){
                            longestPalindrome = new StringBuffer(tempBf);
                        } else if (tempBf.length() > longestPalindrome.length()) {
                            longestPalindrome.setLength(0);
                            longestPalindrome.append(tempBf);
                        }
                    }
                }
            }
        }

        if (longestPalindrome == null) {
            return String.valueOf(s.charAt(0));
        }

        return longestPalindrome.toString().trim();
    }

    private boolean checkIfPalindrome(char[] inputArray, int i, int j) {
        for (int k = i, l = j ; k <= (i+j)/2 ; k++, l--){
            if(inputArray[k] != inputArray[l]){
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        LongestPalindromicSubstring test = new LongestPalindromicSubstring();

        System.out.println(test.longestPalindrome("aaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeeeffffffffffgggggggggghhhhhhhhhhiiiiiiiiiijjjjjjjjjjkkkkkkkkkkllllllllllmmmmmmmmmmnnnnnnnnnnooooooooooppppppppppqqqqqqqqqqrrrrrrrrrrssssssssssttttttttttuuuuuuuuuuvvvvvvvvvvwwwwwwwwwwxxxxxxxxxxyyyyyyyyyyzzzzzzzzzzyyyyyyyyyyxxxxxxxxxxwwwwwwwwwwvvvvvvvvvvuuuuuuuuuuttttttttttssssssssssrrrrrrrrrrqqqqqqqqqqppppppppppoooooooooonnnnnnnnnnmmmmmmmmmmllllllllllkkkkkkkkkkjjjjjjjjjjiiiiiiiiiihhhhhhhhhhggggggggggffffffffffeeeeeeeeeeddddddddddccccccccccbbbbbbbbbbaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeeeffffffffffgggggggggghhhhhhhhhhiiiiiiiiiijjjjjjjjjjkkkkkkkkkkllllllllllmmmmmmmmmmnnnnnnnnnnooooooooooppppppppppqqqqqqqqqqrrrrrrrrrrssssssssssttttttttttuuuuuuuuuuvvvvvvvvvvwwwwwwwwwwxxxxxxxxxxyyyyyyyyyyzzzzzzzzzzyyyyyyyyyyxxxxxxxxxxwwwwwwwwwwvvvvvvvvvvuuuuuuuuuuttttttttttssssssssssrrrrrrrrrrqqqqqqqqqqppppppppppoooooooooonnnnnnnnnnmmmmmmmmmmllllllllllkkkkkkkkkkjjjjjjjjjjiiiiiiiiiihhhhhhhhhhggggggggggffffffffffeeeeeeeeeeddddddddddccccccccccbbbbbbbbbbaaaa"));
    }


}
