package leetcode;

public class MedianOfTwoSortedArray {

    public double findMedianSortedArrays(int[] nums1, int[] nums2) {

        int size = nums1.length + nums2.length;
        int[] combinedArray = new int[size];

        addElementsToArray(nums1, combinedArray, 0);
        addElementsToArray(nums1, combinedArray, nums1.length - 1);

        int combinedArraySize = combinedArray.length;
        if (combinedArraySize % 2 == 0){
            return ((combinedArray[combinedArraySize/2]) + combinedArray[(combinedArraySize/2) - 1])/2;
        } else {
            return combinedArray[combinedArraySize/2];
        }
    }

    private void addElementsToArray(int[] nums, int[] combinedArray, int index) {

        if (combinedArray == null){
            index = 0;
        }

        for (int i = 0; i<nums.length ; i++){
            combinedArray[index] = nums[i];
            index++;
        }
    }
}
