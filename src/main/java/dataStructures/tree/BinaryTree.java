package dataStructures.tree;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by pjaswa1 on 6/22/17.
 */
public class BinaryTree {

    public static void preOrderTraversal(BinaryTreeNode root){

        visit(root);
        if(root.left != null && root.right != null){
            preOrderTraversal(root.left);
            preOrderTraversal(root.right);
        }
    }

    public static void inOrderTraversal(BinaryTreeNode root){

        if(root.left != null){
            inOrderTraversal(root.left);
        }
        visit(root);
        if(root.right != null){
            inOrderTraversal(root.right);
        }
    }

    public static void postOrderTraversal(BinaryTreeNode root){

        if(root.left != null){
            postOrderTraversal(root.left);
        }
        if(root.right != null){
            postOrderTraversal(root.right);
        }
        visit(root);
    }

    public static void levelOrderTraversal(BinaryTreeNode root){

        Queue<BinaryTreeNode> queue = new LinkedList();
        visit(root);
        queue.add(root);
        while (root != null) {
            System.out.println();
            BinaryTreeNode node = queue.remove();
            visit(node.left);
            queue.add(node.left);
            visit(node.right);
            queue.add(node.right);
        }
    }

    private static void visit(BinaryTreeNode root) {
        root.isVisited = true;
        System.out.print(root.item);
    }

    public static void binarySearch(BinaryTreeNode root, int itemToFind){

        if(root.item == itemToFind){
            System.out.println("Item found!");
            visit(root);
        } else if (itemToFind < root.item && root.left != null){
            binarySearch(root.left, itemToFind);
        } else if (itemToFind > root.item && root.right != null){
            binarySearch(root.right, itemToFind);
        }

    }

    public BinaryTreeNode search(BinaryTreeNode root, int key)
    {
        // Base Cases: root is null or key is present at root
        if (root==null || root.item==key)
            return root;

        // val is greater than root's key
        if (root.item > key)
            return search(root.left, key);

        // val is less than root's key
        return search(root.right, key);
    }

    public static boolean isBinarySearchTree(BinaryTreeNode root, int min, int max){

        if(root == null) {
            return true;
        }

        if(root.item <= min || root.item >= max){
            return false;
        }

        return isBinarySearchTree(root.left, min, root.item) && isBinarySearchTree(root.right, root.item, max);
    }

    public static void main(String[] args) {

        BinaryTreeNode root = new BinaryTreeNode(4);

        BinaryTreeNode rightRight = new BinaryTreeNode(7, null, null, null);
        BinaryTreeNode rightLeft = new BinaryTreeNode(5, null, null, null);
        BinaryTreeNode leftRight = new BinaryTreeNode(3, null, null, null);
        BinaryTreeNode leftLeft = new BinaryTreeNode(1, null, null, null);
        BinaryTreeNode right = new BinaryTreeNode(6, rightLeft, rightRight, null);
        BinaryTreeNode left = new BinaryTreeNode(2, leftLeft, leftRight, root);

        root.parent = null;
        root.left = left;
        root.right = right;

        leftLeft.parent = left;
        leftRight.parent = left;

        rightLeft.parent = right;
        rightRight.parent = right;

        leftLeft.left = new BinaryTreeNode(8);
        leftLeft.right = new BinaryTreeNode(9);
        leftRight.left = new BinaryTreeNode(10);
        leftRight.right = new BinaryTreeNode(11);

        rightLeft.left = new BinaryTreeNode(12);
        rightLeft.right = new BinaryTreeNode(13);
        rightRight.left = new BinaryTreeNode(14);
        rightRight.right = new BinaryTreeNode(15);

        System.out.println("Preorder traversal:");
        preOrderTraversal(root);

        System.out.println();
        binarySearch(root, 10);
        System.out.println();

        System.out.println("Inorder traversal:");
        inOrderTraversal(root);

        System.out.println();
        System.out.println("Postorder traversal:");
        postOrderTraversal(root);

        System.out.println();
        System.out.println("LevelOrder traversal:");
        levelOrderTraversal(root);

        System.out.println(isBinarySearchTree(root, Integer.MIN_VALUE, Integer.MAX_VALUE));

    }
}
