package dataStructures.tree;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pjaswa1 on 6/22/17.
 */
public class BinaryTreeNode {

    public int item;
    public BinaryTreeNode left;
    public BinaryTreeNode right;
    public BinaryTreeNode parent;
    public boolean isVisited = false;

    public BinaryTreeNode(int item, BinaryTreeNode left, BinaryTreeNode right, BinaryTreeNode parent) {
        this.item = item;
        this.left = left;
        this.right = right;
        this.parent = parent;
    }

    public BinaryTreeNode(int item){
        this.item = item;
    }

    public BinaryTreeNode(){
        this.item = item;
    }
}
