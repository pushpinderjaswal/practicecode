package dataStructures.arraysAndStrings;

/**
 * Created by pjaswa1 on 6/25/17.
 */
public class OneEditAway {

    public static boolean checkEdit(String str1, String str2){

        if(str2.length() - str1.length() > 1 || str1.length() - str2.length() > 1){
            return false;
        }

        char[] charArray1 = str1.toLowerCase().toCharArray();
        char[] charArray2 = str2.toLowerCase().toCharArray();

        int index2 = 0;
        int index1 = 0;

        boolean[] flag = new boolean[charArray1.length];
        while (index1 < charArray1.length && index2 < charArray1.length){
            for(int i = 0 ; i < charArray2.length; i++){
                if(charArray1[index1] == charArray2[i] && index2 < charArray1.length){
                    flag[index2] = true;
                    charArray2[i] = ' ';
                    index2++;
                }
            }
            index1++;
        }

        int counter = 0;
        for (int i=0; i<flag.length; i++){
            if (flag[i] == false){
                counter++;
            }
            if (counter > 1){
                return false;
            }
        }

        return true;
    }

    public static void main(String[] args) {

        System.out.println(checkEdit("Pale", "palee"));

    }
}
