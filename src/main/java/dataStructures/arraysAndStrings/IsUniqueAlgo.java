package dataStructures.arraysAndStrings;

/**
 * Created by pjaswa1 on 6/23/17.
 */
// Question  - Implement an algorithm to determine if  String has all unique characters. What if you cannot use additional data structures?
public class IsUniqueAlgo {

    public static Boolean isUniqueChars(String string){

        char[] charArray = string.toCharArray();

        for(int i = 0; i < charArray.length ; i++){
            for (int j = i+1 ; j < charArray.length ; j++){
                if (charArray[i] == charArray[j]){
                    return false;
                }
            }
        }
        return true;
    }

    public static void main(String[] args) {

        System.out.println(isUniqueChars("abcdefa"));

    }

}
