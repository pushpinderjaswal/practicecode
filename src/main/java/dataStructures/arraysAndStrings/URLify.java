package dataStructures.arraysAndStrings;

/**
 * Created by pjaswa1 on 6/24/17.
 */
public class URLify {

    public static char[] getUrl(char[] charArray){

        if(charArray != null){
            for (int i=0; i< charArray.length; i++){
                if (charArray[i] == ' '){
                    pushChars(charArray, i);
                    charArray[i] = '%';
                    charArray[i+1] = '2';
                }
            }
        }
        return charArray;
    }

    private static void pushChars(char[] charArray, int i) {

        for (int j = charArray.length - 2; j >= i ; j--){
            int counter = 0;
            if(charArray[j] != ' '){
                counter++;
            }
            if(counter > 0){
                charArray[j+1] = charArray[j];
                charArray[j] = charArray[j-1];
                charArray[j] = ' ';
            }

        }
    }

    public static void main(String[] args) {

        char[] chars = {'M','r',' ','J','o','h','n',' ','S','m','i','t','h',' ',' '};

        System.out.println(getUrl(chars));
    }
}
