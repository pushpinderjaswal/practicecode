package dataStructures.arraysAndStrings;

/**
 * Created by pjaswa1 on 6/25/17.
 * TODO: In place
 */
public class RotateMatrix {

    public static int[][] createMatrix(int i, int j){

        int[][] metrix = new  int[i][j];
        int count = 1;

        for (int k = 0 ; k < i ; k ++){
            for (int l = 0; l< j ; l ++){
                metrix[k][l] = count;
                count++;
            }
        }

        return metrix;

    }

    public static int[][] rotateMatrix (int[][] input, int i, int j){

        int[][] metrix = new int[i][j];

        for (int k = 0, m = 0 ; k < i ; k++, m++){
            for (int l = 0, n = i - 1; l< j ; l ++ , n--){
//                int temp = input[k][l];
//                input[k][l] = input[n][m];
//                input[n][m] = temp;
                metrix[k][l] = input[n][m];
            }
        }

        return metrix;
    }

    public static void main(String[] args) {

        int[][] metrix = createMatrix(3,3);

        for (int k = 0 ; k < 3 ; k ++){
            for (int l = 0; l< 3 ; l ++){
                System.out.print(metrix[k][l]);
                System.out.print("  ");
            }
            System.out.println();
        }

        System.out.println();
        int[][] rotatedMetrix = rotateMatrix(metrix,3,3);

        for (int k = 0 ; k < 3 ; k ++){
            for (int l = 0; l< 3 ; l ++){
                System.out.print(rotatedMetrix[k][l]);
                System.out.print("  ");
            }
            System.out.println();
        }

    }
}
