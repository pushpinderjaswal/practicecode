package dataStructures.arraysAndStrings;

/**
 * Created by pjaswa1 on 6/25/17.
 */
public class ZeroRowColMatrix {

    public static int[][] makeRowColZero (int[][] matrix, int i, int j){

        for (int k = 0 ; k < i ; k ++){
            for (int l = 0; l< j ; l ++){
                if(matrix[k][l] == 0){
                    return setRowColZero(matrix, k, l);
                }
            }
        }

        return matrix;
    }

    private static int[][] setRowColZero(int[][] matrix, int k, int l) {

        for(int i = 0; i < matrix.length ; i++){
            matrix[k][i] = 0;
        }
        for(int i = 0; i < matrix.length ; i++){
            matrix[i][l] = 0;
        }
        return matrix;
    }

    public static int[][] createMatrix(int i, int j){

        int[][] metrix = new  int[i][j];
        int count = 1;

        for (int k = 0 ; k < i ; k ++){
            for (int l = 0; l< j ; l ++){
                metrix[k][l] = count;
                if (count == 6){
                    metrix[k][l] = 0;
                }
                count++;
            }
        }

        return metrix;

    }

    public static void main(String[] args) {


        int[][] result = makeRowColZero(createMatrix(3,3), 3,3 );

        for (int k = 0 ; k < 3 ; k ++){
            for (int l = 0; l< 3 ; l ++){
                System.out.print(result[k][l]);
                System.out.print("  ");
            }
            System.out.println();
        }
    }
}
