package dataStructures.arraysAndStrings;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by pjaswa1 on 6/24/17.
 */
public class PalindromePermutation {

    public static Set<String> getPermutations(String input){

        Set<String> permutations = new HashSet<String>();
        if(input == null){
            return null;
        }else if (input.length() == 0) {
            permutations.add("");
            return permutations;
        }

        char initial = input.charAt(0);
        String rem = input.substring(1);
        Set<String> words = getPermutations(rem);
        for(String strNew: words){
            for (int i=0; i<=strNew.length(); i++){
                permutations.add(charInsert(strNew, initial, i));
            }
        }

        return permutations;

    }

    public static boolean checkPalindrome(String input){

        Set<String> permutations = getPermutations(input);
        System.out.println(permutations);

        for (String perm : permutations){
            if(isPalindrome(perm)){
                return true;
            }
        }
        return false;
    }

    public static boolean isPalindrome(String input){
        char[] charArray = input.toCharArray();
        for (int i=0, j = input.length() -1 ; i < input.length()/2 ; i++, j--){
            if (charArray[i] != charArray[j]){
                return false;
            }
        }
        return true;
    }

    private static String charInsert(String strNew, char initial, int i) {

        String begin = strNew.substring(0,i);
        String end = strNew.substring(i);
        return begin + initial + end;
    }

    public static void main(String[] args) {

        System.out.println(checkPalindrome("abc"));

    }

}
