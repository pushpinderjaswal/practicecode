package dataStructures.arraysAndStrings;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class FindDups {

    public Map<Integer, Integer> findDups(int[] inputArray){

        Map<Integer, Integer> map = new HashMap<Integer, Integer>();

        if (inputArray.equals(null)){
            return map;
        }
        for (int i = 0 ; i<inputArray.length ; i++){
            for (int j = i+1 ; j<inputArray.length ; j++){
                if (inputArray[i] == inputArray[j]){
                    if (map.containsKey(inputArray[i])){
                        map.put(inputArray[i], map.get(inputArray[i]) + 1);
                    }
                    map.put(inputArray[i], 1);
                }
            }
        }
        return map;
    }

    public Set<Integer> findDups2(int[] inputArray){

        Set<Integer> set = new HashSet<Integer>();

        for (int i = 0 ; i < inputArray.length ; i++){
            if(!set.add(inputArray[i])){
                System.out.println(inputArray[i]);
            }
        }
       return set;
    }

    public Map<Integer, Integer> findDups3(int[] inputArray){

        Map<Integer, Integer> map = new HashMap<Integer, Integer>();

        for (int i = 0; i < inputArray.length ; i++){
            if (map.containsKey(inputArray[i])){
                map.put(inputArray[i], map.get(inputArray[i]) + 1);
            } else {
                map.put(inputArray[i], 1);
            }
        }

        return map;
    }

    public static void main(String[] args) {

        FindDups findDups = new FindDups();
        int[] inputArray = {1,2,3,4,5,6,7,2,6,4};

        Map<Integer, Integer> map = findDups.findDups(inputArray);

        for (Map.Entry<Integer,Integer> item : map.entrySet()){
            System.out.println("Key = " + item.getKey() + " value = " + item.getValue());
        }

        findDups.findDups2(inputArray);

        Map<Integer, Integer> map2 = findDups.findDups3(inputArray);

        for (Map.Entry<Integer,Integer> item : map2.entrySet()){
            System.out.println("Key = " + item.getKey() + " value = " + item.getValue());
        }
    }
}
