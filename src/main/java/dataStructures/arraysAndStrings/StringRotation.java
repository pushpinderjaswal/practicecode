package dataStructures.arraysAndStrings;

/**
 * Created by pjaswa1 on 6/25/17.
 */
public class StringRotation {

    public static boolean isString(String input1, String input2){

        if(input1.equals(null)){
            return false;
        }

        char[] charArray1 = input1.toCharArray();
        char[] charArray2 = input2.toCharArray();


        for (int i=0; i< charArray1.length ; i++){

            for (int j = 0; j < charArray2.length ; j++){
                if(charArray1[i] == charArray2[j]){
                    return matchString(charArray1, charArray2, i, j);
                }
            }

        }

        return false;
    }

    private static boolean matchString(char[] charArray1, char[] charArray2, int i, int j) {

        int pointer = 0;
        for (int m= i, n =j; n < charArray2.length && m < charArray1.length ; m++, n++ ){
            if(charArray1[m] != charArray2[n]){
                return false;
            }
            pointer = m+1;
        }
        for (int m= pointer, n =0; n < j; m++, n++ ){
            if(charArray1[m] != charArray2[n]){
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {

        System.out.println(isString("abcd", "abcdee"));

    }
}
