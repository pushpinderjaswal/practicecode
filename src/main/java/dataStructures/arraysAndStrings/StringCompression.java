package dataStructures.arraysAndStrings;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pjaswa1 on 6/25/17.
 *
 * aaabbb -> a3b3
 */
public class StringCompression {

    public static StringBuilder compress(String input){

        char[] charArray = input.toCharArray();
        Map<Character, Integer> map = new HashMap<Character, Integer>();

        char curr;
        char prev;

        for (int i=1; i<charArray.length; i++){

            map.put(charArray[i],1);

            curr = charArray[i];
            prev = charArray[i-1];

            if(curr == prev){
                int sum = map.get(charArray[i]);
                map.put(charArray[i], sum +1);
            }
        }



        return mapToString(map);
    }

    public static StringBuilder mapToString(Map<Character, Integer> map){

        StringBuilder sb = new StringBuilder();

        for (Map.Entry<Character, Integer> entry : map.entrySet()){
            sb.append(entry.getKey());
            sb.append(entry.getValue());
        }

        return sb;
    }

    public static void main(String[] args) {

        System.out.println(compress("aasbbcc"));


    }

}
