package dataStructures.arraysAndStrings;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pjaswa1 on 6/23/17.
 * Question: Given two strings, write a method to decide if one is a permutation of the other.
 */
public class CheckPermutation {

    public static Boolean checkPermutation(String string1, String string2){

        if (string1.length() != string2.length()){
            return false;
        }
        char[] str1Array = string1.toCharArray();
        char[] str2Array = string2.toCharArray();

        List<Character> list1 = getCharToList(str1Array);
        List<Character> list2 = getCharToList(str2Array);

        for (int i=0; i< list1.size()  ; i ++){
            for (int j=0; j < list2.size() ; j ++){
                if(list1.get(i) == list2.get(j)){
                    list2.remove(j);
                }
            }
        }

        if (list2.size() == 0){
            return true;
        }

        return false;
    }

    public static boolean checkPermutation1(String str1, String str2){



        return true;
    }

    private static List<Character> getCharToList(char[] array) {

        List<Character> list = new ArrayList<Character>();

        for (int i = 0; i < array.length ; i++){
            list.add(array[i]);
        }
        return list;
    }

    public static void main(String[] args) {

        System.out.println(checkPermutation("doggg", "godd"));

    }
}
