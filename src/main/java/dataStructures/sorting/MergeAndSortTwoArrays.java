package dataStructures.sorting;

import java.util.Arrays;

public class MergeAndSortTwoArrays {

    QuickSort quickSort = new QuickSort();

    public int[] merge(int[] array1, int[] array2) {

        int length1 = array1.length - 1;
        int length2 = array2.length - 1;

        for (int i = length1 - length2, j = 0; i < array1.length ; i ++, j++) {
            array1[i] = array2[j];
        }

        return sortArray(array1);

    }

    private int[] sortArray(int[] array1) {
        int left = 0;
        int right = array1.length - 1;
        return quickSort.sort(array1, left, right);
    }

    public static void main(String[] args) {

        MergeAndSortTwoArrays mergeAndSortTwoArrays = new MergeAndSortTwoArrays();

        int[] array1 = {3,5,3,4,3,0,0,0,0,0};
        int[] array2 = {1,6,3,9,4};


        System.out.println(Arrays.toString(mergeAndSortTwoArrays.merge(array1, array2)));


    }

}
