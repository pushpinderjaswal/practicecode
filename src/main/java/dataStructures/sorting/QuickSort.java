package dataStructures.sorting;

import java.util.Arrays;

public class QuickSort {

    public int[] sort(int[] input, int left, int right) {

        int index = partition(input, left, right);
        if (left < index - 1) {
            sort(input, left, index - 1);
        }
        if (right > index) {
            sort(input, index, right);
        }
        return input;
    }

    private int partition(int[] input, int left, int right) {

        int pivot = input[(left + right) / 2];
        while (left <= right) {
            while (input[left] < pivot) {
                left++;
            }
            while (input[right] > pivot) {
                right--;
            }
            if (left <=  right) {
                int temp = input[left];
                input[left] = input[right];
                input[right] = temp;
                left++;
                right--;
            }
        }
        return left;
    }

    public static void main(String[] args) {

        QuickSort quickSort = new QuickSort();

        int[] input = {10,2,6,1,8,4,2,8,4,6};
        System.out.println(Arrays.toString(quickSort.sort(input, 0, input.length-1)));

    }
}
