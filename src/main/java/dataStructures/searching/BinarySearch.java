package dataStructures.searching;

/**
 * Binary Search
 * Worst case performance: O(log n)
 * Best case performance: O(1)
 */
public class BinarySearch {

    public int search(int[] input, int item){

        if(input != null){
            int start = 0;
            int end = input.length - 1;

            while (start <= end){
                int mid = (start + end)/2;
                if (input[mid] == item){
                    System.out.println("Item found");
                    return mid;
                }
                if (item < input[mid]){
                    end = mid - 1;
                } else if (item > input[mid]){
                    start = mid + 1;
                }
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        BinarySearch binarySearch = new BinarySearch();
        int[] inputArray = {0,1,2,3,4,5,6,7,8,9,10,25};
        System.out.println(binarySearch.search(inputArray, 26));
    }
}
