package dataStructures.graphs;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pjaswa1 on 7/3/17.
 */
public class GraphNode {

    public Object item;
    public List<GraphNode> children = new ArrayList<>();
    GraphNode parent;
    public boolean visited = false;


    public GraphNode(Object item, List<GraphNode> graphNodeList, GraphNode parent) {
        this.item = item;
        this.children = graphNodeList;
        this.parent = parent;
    }

    public GraphNode(Object item) {
        this.item = item;
    }
}
