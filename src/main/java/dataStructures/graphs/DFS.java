package dataStructures.graphs;

public class DFS {

    public void depthFirstSearch(GraphNode root){

        if (root == null) {
            return;
        }
        visit(root);
        for (GraphNode node : root.children) {
            if (!node.visited){
                depthFirstSearch(node);
            }
        }
    }

    public void visit(GraphNode root) {
        System.out.println(root.item);
        root.visited = true;
    }

    public static void main(String[] args) {

        DFS dfs = new DFS();

        GraphNode one = new GraphNode(1);
        GraphNode two = new GraphNode(2);
        GraphNode three = new GraphNode(3);
        GraphNode four = new GraphNode(4);
        GraphNode five = new GraphNode(5);
        GraphNode six = new GraphNode(6);
        GraphNode seven = new GraphNode(7);
        GraphNode eight = new GraphNode(8);

        one.children.add(two);
        one.children.add(four);
        one.children.add(six);

        two.children.add(three);
        four.children.add(five);
        six.children.add(seven);

        three.children.add(five);
        three.children.add(two);

        five.children.add(three);
        five.children.add(four);
        five.children.add(eight);

        seven.children.add(eight);
        seven.children.add(six);

        eight.children.add(five);
        eight.children.add(seven);

        dfs.depthFirstSearch(one);
    }
}
