package dataStructures.graphs;



import java.util.*;

/**
 * Created by pjaswa1 on 7/3/17.
 */
public class BFS {
    Queue<GraphNode> graphNodeQueue = new LinkedList<GraphNode>();
    Map<Object,Boolean> map = new HashMap<Object, Boolean>();
    DFS dfs = new DFS();

    public void travaerse(GraphNode startNode) {

        if (!startNode.children.isEmpty() && !isVisited(startNode)) {
            for(GraphNode graphNode : startNode.children){
                graphNodeQueue.add(graphNode);
            }
        }

        dfs.visit(graphNodeQueue.peek());
        GraphNode node = graphNodeQueue.remove();
        travaerse(node);

    }


    public void breadthFirstSearch(GraphNode root, Object input){

        Queue<GraphNode> queue = new LinkedList<GraphNode>();
        dfs.visit(root);
        queue.add(root);

        while (!queue.isEmpty()) {
            GraphNode node = queue.remove();
            for (GraphNode item : node.children) {
                if (!item.visited) {
                    dfs.visit(item);
                    queue.add(item);
                }
            }
        }
    }

    private boolean isVisited(GraphNode graphNode) {
        if(!map.containsKey(graphNode.item)){
            map.put(graphNode.item, true);
            return false;
        }
        return true;
    }

    public static void main(String[] args) {

        BFS bfs = new BFS();

        GraphNode one = new GraphNode(1);
        GraphNode two = new GraphNode(2);
        GraphNode three = new GraphNode(3);
        GraphNode four = new GraphNode(4);
        GraphNode five = new GraphNode(5);
        GraphNode six = new GraphNode(6);
        GraphNode seven = new GraphNode(7);
        GraphNode eight = new GraphNode(8);

        one.children.add(two);
        one.children.add(four);
        one.children.add(six);

        two.children.add(three);
        four.children.add(five);
        six.children.add(seven);

        three.children.add(five);
        three.children.add(two);

        five.children.add(three);
        five.children.add(four);
        five.children.add(eight);

        seven.children.add(eight);
        seven.children.add(six);

        eight.children.add(five);
        eight.children.add(seven);

        bfs.breadthFirstSearch(one, 8);

    }
}
