package dataStructures.stack;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pjaswa1 on 6/22/17.
 */
public class Stack {

    static List<Integer> stack;
    int min;

    public  void push(Integer item){

        if (stack == null){
            stack = new ArrayList<Integer>();
            stack.add(item);
            min = item;
        }else {
            stack.add(item);
            if(item < min){
                min = item;
            }
        }
    }

    public static void pop(){
        stack.remove(stack.size() - 1);
    }

    public int getMin(){
        return min;
    }

    public static void visit(){
        System.out.println(stack.get(stack.size() - 1));
    }

    public static void main(String[] args) {

        Stack stack = new Stack();

        stack.push(4);
        stack.push(2);
        stack.push(1);
        stack.push(3);

        visit();

        visit();

        System.out.println(stack.getMin());
    }
}
