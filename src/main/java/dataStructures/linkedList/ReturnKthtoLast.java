package dataStructures.linkedList;

/**
 * Created by pjaswa1 on 7/16/17.
 */
public class ReturnKthtoLast {

    public static LinkedListNode kthToLast(LinkedListNode node, int kth){

        while (kth != 0){
            node = node.nextNode;
            kth--;
        }

        return node;

    }

    // Book Solution
    public static LinkedListNode kthToLast2(LinkedListNode node, int k){

        LinkedListNode p1 = node;
        LinkedListNode p2 = node;

        int i = 0;
        while (i < k ){
            p1 = p1.nextNode;
            i++;
        }
        while (p1 != null){
            p1 = p1.nextNode;
            p2 = p2.nextNode;
        }

        return p2;

    }

    public static void main(String[] args) {
        LinkedListNode node1 = new LinkedListNode(1);
        LinkedListNode node2 = new LinkedListNode(2);
        LinkedListNode node3 = new LinkedListNode(3);
        LinkedListNode node4 = new LinkedListNode(4);
        LinkedListNode node5 = new LinkedListNode(5);
        LinkedListNode node6 = new LinkedListNode(6);

        node1.nextNode = node2;
        node2.nextNode = node3;
        node3.nextNode = node4;
        node4.nextNode = node5;
        node5.nextNode = node6;
        node6.nextNode = null;

        LinkedListNode result = kthToLast(node1,2);
        while (result.nextNode != null){
            System.out.println(result.item);
            result = result.nextNode;
        }
        System.out.println(result.item);

        System.out.println();

        LinkedListNode result2 = kthToLast2(node1,2);
        while (result2.nextNode != null){
            System.out.println(result2.item);
            result2 = result2.nextNode;
        }
        System.out.println(result2.item);

    }
}
