package dataStructures.linkedList;

/**
 * Created by pjaswa1 on 7/16/17.
 */
public class SumLists {

    public static LinkedListNode sum(LinkedListNode list1, LinkedListNode list2){

        LinkedListNode newList = null;
        boolean carryOver = false;
        LinkedListNode newListStart = null;

        while (list1 != null){
            int sum = list1.item + list2.item;
            if (carryOver){
                sum = sum + 1;
                carryOver = false;
            }
            if(sum > 9){
                carryOver = true;
                if (newList == null){
                    newList = new LinkedListNode(sum - 10);
                    newListStart = newList;
                } else {
                    newList.nextNode = new LinkedListNode(sum - 10);
                    newList = newList.nextNode;
                }
            } else {
                if (newList == null){
                    newList = new LinkedListNode(sum);
                    newListStart = newList;
                } else{
                    newList.nextNode = new LinkedListNode(sum);
                    newList = newList.nextNode;
                }
            }
            list1 = list1.nextNode;
            list2 = list2.nextNode;
        }
        return newListStart;
    }

    public static void main(String[] args) {

        LinkedListNode node1 = new LinkedListNode(7);
        LinkedListNode node2 = new LinkedListNode(1);
        LinkedListNode node3 = new LinkedListNode(6);

        LinkedListNode node4 = new LinkedListNode(5);
        LinkedListNode node5 = new LinkedListNode(9);
        LinkedListNode node6 = new LinkedListNode(2);

        node1.nextNode = node2;
        node2.nextNode = node3;
        node3.nextNode = null;

        node4.nextNode = node5;
        node5.nextNode = node6;
        node6.nextNode = null;


        LinkedListNode result = sum(node1,node4);
        while (result.nextNode != null){
            System.out.println(result.item);
            result = result.nextNode;
        }
        System.out.println(result.item);

    }

}
