package dataStructures.linkedList;

/**
 * Created by pjaswa1 on 7/16/17.
 */
public class LoopDetection {

    public  static LinkedListNode detectLoop(LinkedListNode node){

        LinkedListNode node1 = node;
        LinkedListNode runner = node;

        while(node1.nextNode != runner.nextNode){
            node1 = node1.nextNode;
            runner = runner.nextNode.nextNode;
        }

        return node1;

    }

    public static void main(String[] args) {
        LinkedListNode node1 = new LinkedListNode(1);
        LinkedListNode node2 = new LinkedListNode(2);
        LinkedListNode node3 = new LinkedListNode(3);
        LinkedListNode node4 = new LinkedListNode(4);
        LinkedListNode node5 = new LinkedListNode(5);
        LinkedListNode node6 = new LinkedListNode(6);

        node1.nextNode = node2;
        node2.nextNode = node3;
        node3.nextNode = node4;
        node4.nextNode = node5;
        node5.nextNode = node6;
        node6.nextNode = node1;

        LinkedListNode result = detectLoop(node1);

        System.out.println(result.item);
    }
}
