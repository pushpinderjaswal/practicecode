package dataStructures.linkedList;

/**
 * Created by pjaswa1 on 7/12/17.
 */
public class RemoveDups {


    public static LinkedListNode removeDups(LinkedListNode node, LinkedListNode startNode){
        if(node.nextNode == null){
            System.out.println("Only one element");
            return node;
        }
        LinkedListNode linkedListNode = node;
        while(node.nextNode != null){
            int counter = 0;
            if(linkedListNode.item == node.nextNode.item && linkedListNode.nextNode.nextNode !=null){
                node.nextNode = node.nextNode.nextNode;
                counter++;
            }
            if (counter == 0){
                node = node.nextNode;
            }
        }
        removeDups(linkedListNode.nextNode, startNode);
        return startNode;
    }

    // Solution from book
    public static LinkedListNode removeDups2(LinkedListNode node){
        LinkedListNode start = node;
        LinkedListNode current = node;
        while (current.nextNode != null){
            LinkedListNode runner = current.nextNode;
            while (runner.nextNode != null){
                if(current.item == runner.item){
                    runner.nextNode = runner.nextNode.nextNode;
                } else {
                    runner = runner.nextNode;
                }
            }
            current = current.nextNode;
        }
        return start;
    }

    public static void main(String[] args) {

        LinkedListNode node1 = new LinkedListNode(1);
        LinkedListNode node2 = new LinkedListNode(2);
        LinkedListNode node3 = new LinkedListNode(3);
        LinkedListNode node4 = new LinkedListNode(1);
        LinkedListNode node5 = new LinkedListNode(5);
        LinkedListNode node6 = new LinkedListNode(6);

        node1.nextNode = node2;
        node2.nextNode = node3;
        node3.nextNode = node4;
        node4.nextNode = node5;
        node5.nextNode = node6;
        node6.nextNode = null;

        LinkedListNode result = removeDups(node1, node1);

        while (result.nextNode != null){
            System.out.println(result.item);
            result = result.nextNode;
        }
        System.out.println(result.item);

        LinkedListNode result2 = removeDups2(node1);

        while (result2.nextNode != null){
            System.out.println(result2.item);
            result2 = result2.nextNode;
        }
        System.out.println(result2.item);

    }

}
