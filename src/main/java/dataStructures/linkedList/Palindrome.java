package dataStructures.linkedList;

import java.util.Stack;

/**
 * Created by pjaswa1 on 7/16/17.
 */
public class Palindrome {

    public static boolean checkPalindrome(LinkedListNode node){

        LinkedListNode mid = node;
        LinkedListNode end = node;
        Stack<Integer> stack = new Stack<Integer>();
        int counter = 1;

        while (end.nextNode != null){
            stack.push(mid.item);
            mid = mid.nextNode;
            end = end.nextNode.nextNode;
            counter++;
            if (end == null){
                break;
            }
        }
        if (counter % 2 == 1){
            mid = mid.nextNode;
        }
        while (mid.nextNode != null){
            int item = stack.pop();
            if (item != mid.item){
                return false;
            }
            mid = mid.nextNode;
        }
        return true;
    }

    public static void main(String[] args) {

        LinkedListNode node1 = new LinkedListNode(3);
        LinkedListNode node2 = new LinkedListNode(2);
        LinkedListNode node3 = new LinkedListNode(1);
        LinkedListNode node4 = new LinkedListNode(3);
        LinkedListNode node5 = new LinkedListNode(3);


        node1.nextNode = node2;
        node2.nextNode = node3;
        node3.nextNode = node4;
        node4.nextNode = node5;
        node5.nextNode = null;
//        node6.nextNode = null;

        System.out.println(checkPalindrome(node1));
    }
}
