package dataStructures.linkedList;

/**
 * Created by pjaswa1 on 7/16/17.
 */
public class DeleteMiddleNode {

    public static LinkedListNode deleteMidNode(LinkedListNode node){

        LinkedListNode p1 = node;
        LinkedListNode p2 = node;
        LinkedListNode p1Start = p1;

        while (p2.nextNode != null) {
            p2 = p2.nextNode.nextNode;
            if (p2 == null || p2.nextNode == null){
                break;
            }
            p1 = p1.nextNode;
        }
        p1.nextNode = p1.nextNode.nextNode;
        return p1Start;
    }

    public static void main(String[] args) {
        LinkedListNode node1 = new LinkedListNode(1);
        LinkedListNode node2 = new LinkedListNode(2);
        LinkedListNode node3 = new LinkedListNode(3);
        LinkedListNode node4 = new LinkedListNode(4);
        LinkedListNode node5 = new LinkedListNode(5);
        LinkedListNode node6 = new LinkedListNode(6);
//        LinkedListNode node7 = new LinkedListNode(7);

        node1.nextNode = node2;
        node2.nextNode = node3;
        node3.nextNode = node4;
        node4.nextNode = node5;
        node5.nextNode = node6;
        node6.nextNode = null;
//        node7.nextNode = null;

        LinkedListNode result = deleteMidNode(node1);

        while (result.nextNode != null){
            System.out.println(result.item);
            result = result.nextNode;
        }
        System.out.println(result.item);
    }

}
