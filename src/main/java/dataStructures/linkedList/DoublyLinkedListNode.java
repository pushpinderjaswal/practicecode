package dataStructures.linkedList;


public class DoublyLinkedListNode {

    public int key;
    public String value;
    public DoublyLinkedListNode next;
    public DoublyLinkedListNode previous;


    public DoublyLinkedListNode(int key, String value) {
        this.key = key;
        this.value = value;
    }
}
