package dataStructures.linkedList;

/**
 * Created by pjaswa1 on 6/21/17.
 */
public class LinkedListNode {

    public int item;
    public LinkedListNode nextNode;

    public LinkedListNode(int item, LinkedListNode nextNode) {
        this.item = item;
        this.nextNode = nextNode;
    }

    public LinkedListNode(int item) {
        this.item = item;
    }

    public LinkedListNode() {
    }
}
