package dataStructures.linkedList;

/**
 * Created by pjaswa1 on 7/16/17.
 */
public class Intersection {

    public static void findIntersection(LinkedListNode list1, LinkedListNode list2){

        LinkedListNode list2Start = list2;

        while (list1.nextNode != null){
            list2 = list2Start;
            while (list2.nextNode != null){
                if(list1.item == list2.item && list1.nextNode == list2.nextNode){
                    System.out.println("Found: " + list1.item);
                }
                list2 = list2.nextNode;
            }
            list1 = list1.nextNode;
        }
    }

    public static void main(String[] args) {
        LinkedListNode node1 = new LinkedListNode(1);
        LinkedListNode node2 = new LinkedListNode(2);
        LinkedListNode node3 = new LinkedListNode(3);
        LinkedListNode node4 = new LinkedListNode(4);
        LinkedListNode node5 = new LinkedListNode(5);

        LinkedListNode node6 = new LinkedListNode(10);
        LinkedListNode node7 = new LinkedListNode(11);
        LinkedListNode node8 = new LinkedListNode(3);
        LinkedListNode node9 = new LinkedListNode(4);
        LinkedListNode node10 = new LinkedListNode(5);

        node1.nextNode = node2;
        node2.nextNode = node3;
        node3.nextNode = node4;
        node4.nextNode = node5;
        node5.nextNode = null;

        node6.nextNode = node7;
        node7.nextNode = node8;
        node8.nextNode = node3;


        findIntersection(node1, node6);
    }
}
