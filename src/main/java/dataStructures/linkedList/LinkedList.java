package dataStructures.linkedList;

/**
 * Created by pjaswa1 on 6/21/17.
 */
public class LinkedList {



    public static Object get(int item, LinkedListNode linkedListNode){

        System.out.println("traversing: " + linkedListNode.item);
        if(linkedListNode.nextNode != null){
            if(linkedListNode.item == item){
                System.out.println("Found");
                return item;
            } else {
                get(item, linkedListNode.nextNode);
            }
        }

        return "Not Found";
    }

    public static LinkedListNode addNode(LinkedListNode newNode, LinkedListNode linkedList){

        int counter = 0;
        LinkedListNode startNode = null;

        if(counter < 1){
            startNode = linkedList;
            counter++;
        }


        if(linkedList.nextNode == null){
            linkedList.nextNode = newNode;
        } else if (linkedList.nextNode != null){
            addNode(newNode, linkedList.nextNode);
        }

        return startNode;
    }

    public static LinkedListNode addNodeInBetween(LinkedListNode newNode, LinkedListNode linkedList, int itemAddAfter){

        int counter = 0;
        LinkedListNode startNode = null;

        if(counter < 1){
            startNode = linkedList;
            counter++;
        }

        if(linkedList.item == itemAddAfter){
            LinkedListNode nextNode = linkedList.nextNode;
            linkedList.nextNode = newNode;
            newNode.nextNode = nextNode;
        } else if (linkedList.nextNode != null){
            addNodeInBetween(newNode, linkedList.nextNode,itemAddAfter);
        }

        return startNode;
    }

    public static LinkedListNode deleteNodeInBetween(LinkedListNode linkedList, int itemDeleteAfter){

        int counter = 0;
        LinkedListNode startNode = null;

        if(counter < 1){
            startNode = linkedList;
            counter++;
        }

        if(linkedList.item == itemDeleteAfter){
            LinkedListNode nextNode = linkedList.nextNode.nextNode;
            linkedList.nextNode.nextNode = null;
            linkedList.nextNode = nextNode;
        } else if (linkedList.nextNode != null){
            deleteNodeInBetween(linkedList.nextNode,itemDeleteAfter);
        }

        return startNode;
    }

    public static void traverseLinkedList(LinkedListNode linkedListNode){

        System.out.println(linkedListNode.item);
        if (linkedListNode.nextNode != null){
            traverseLinkedList(linkedListNode.nextNode);
        }
    }

    public static void main(String[] args) {


        LinkedListNode node3 = new LinkedListNode(3, null);
        LinkedListNode node2 = new LinkedListNode(2, node3);
        LinkedListNode node1 = new LinkedListNode(1, node2);

        LinkedListNode node4 = new LinkedListNode(4, null);
        LinkedListNode node5 = new LinkedListNode(5, null);


        System.out.println(get(2, node1));

        LinkedListNode linkedListNode = addNode(node4, node1);
        System.out.println("Traversing:");
        traverseLinkedList(linkedListNode);

        System.out.println();
        System.out.println();
        LinkedListNode startNode = addNodeInBetween(node5, node1, 2);
        System.out.println("Traversing add node in between");
        traverseLinkedList(startNode);

        System.out.println();
        System.out.println();
        LinkedListNode startNode1 = deleteNodeInBetween(node1, 2);
        System.out.println("Traversing delete node in between");
        traverseLinkedList(startNode);

    }
}
