package dataStructures.linkedList;

/**
 * Created by pjaswa1 on 7/16/17.
 */
public class Partition {

    public static LinkedListNode partitionList(LinkedListNode node, int item){

        LinkedListNode startNode = node;

        while(node.nextNode.item != item){
            node = node.nextNode;
        }

        LinkedListNode itemNode = node.nextNode;

        node.nextNode = node.nextNode.nextNode;
        itemNode.nextNode = null;

        return orderList(startNode, item, itemNode);


    }

    private static LinkedListNode orderList(LinkedListNode startNode, int item, LinkedListNode itemNode) {

        LinkedListNode prev = new LinkedListNode(0);
        LinkedListNode next = itemNode;
        LinkedListNode prevStart = prev;

        while(startNode.nextNode != null){
            if(startNode.item <= item){
                prev.nextNode = new LinkedListNode(startNode.item);
                prev = prev.nextNode;
                startNode = startNode.nextNode;
            } else if(startNode.item > item){
                itemNode.nextNode = new LinkedListNode(startNode.item);
                itemNode = itemNode.nextNode;
                startNode = startNode.nextNode;
            }
        }

        if(startNode.item <= item){
            prev.nextNode = new LinkedListNode(startNode.item);
            prev = prev.nextNode;
        } else if(startNode.item > item){
            itemNode.nextNode = new LinkedListNode(startNode.item);
            itemNode = itemNode.nextNode;
        }

        itemNode.nextNode = null;
        prev.nextNode = next;

        return prevStart;
    }


    public static void main(String[] args) {
        LinkedListNode node1 = new LinkedListNode(6);
        LinkedListNode node2 = new LinkedListNode(5);
        LinkedListNode node3 = new LinkedListNode(3);
        LinkedListNode node4 = new LinkedListNode(2);
        LinkedListNode node5 = new LinkedListNode(4);
        LinkedListNode node6 = new LinkedListNode(1);

        node1.nextNode = node2;
        node2.nextNode = node3;
        node3.nextNode = node4;
        node4.nextNode = node5;
        node5.nextNode = node6;
        node6.nextNode = null;

        LinkedListNode result = partitionList(node1, 3);

        while (result.nextNode != null){
            System.out.println(result.item);
            result = result.nextNode;
        }
        System.out.println(result.item);
    }
}
